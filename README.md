# Sportvorschrift für das Heer
## Illustrations

Folder Structure:

* raw_figures
  * Images of all the illustrations from the original book scan
* bw_figures
  * A conversion of the illustrations with their color and background removed
* svg_figures
  * A conversion of the illustrations to scalable vector graphics

### The Plan

#### Phase 1: Collection of Material - Complete

* Rip PNGs of Images
* Transcribe the text into an unaltered German version

#### Phase 2: Elaboration of Material

* Clean up/redraw PNGs
* Translate the German Text
* Design a cover for the book

#### Phase 3: Profit

* Put everything together into the best book /pol/ has ever produced

